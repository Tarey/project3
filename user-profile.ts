import express/* , {Request, Response} */ from 'express'
import { db } from './db'

// profileRoute is used in main.ts
export let profileRoute = express.Router()


profileRoute.get('/login', main)


async function main(req: express.Request, res: express.Response){
    const result = await db.query(`select * from users`);
    const usersFromDB = result.rows;
    console.log(usersFromDB);

    res.json(usersFromDB)
}