import fetch from 'node-fetch'
import { chatmarker, GoogleResult, User } from '../models/user'
import { Knex } from 'knex'

export class UserService {
  constructor(private knex: Knex) {}
  
  // obtain user's information
  async getUserByUserName(username: string): Promise<User> {
    return this.knex
      .select('*')
      .from('users')
      .where({ username })
      .first()
  }

  // get all information from the table usersloc to be displayed on the map with or without logged in
  async getUserLoc(): Promise<void> {
    return this.knex
      .select('*')
      .from('usersloc')
  }

  // get all chat markers information globally
  async getChatMarkerInformation(): Promise<chatmarker[]> {
    const result = await this.knex.raw(`SELECT * FROM chatmarker`)
    return result;
  }

  // add new chat markers
  async addChatMarker(body: { }){
    const {} = body;
    const result = await this.knex.raw(`INSERT INTO chatmarker () VALUES ($1, $2)`,
      [],
    )
    return result;
  }
  
  // update existing chat markers
  // async updateChatmarker(chatlocation: Geolocation, body: chatmarker) {
  //   const { messages_usernames } = body;
  //   const result = await this.knex.raw(`UPDATE chatmarker SET messages_usernames = $1 WHERE chatlocation = $2`,
  //     [messages_usernames, chatlocation],
  //   )
  //   return result;
  // }

  async createOrUpdateGoogleUser(accessToken: string): Promise<User> {
    const fetchRes = await fetch(
      'https://www.googleapis.com/oauth2/v2/userinfo',
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      },
    )
    const fetchResult: GoogleResult = await fetchRes.json()

    let user: User = await this.knex
      .select('*')
      .from('users')
      .where({ email: fetchResult.email })
      .first()

    // if this user is new comer
    if (!user) {
      let users = await this.knex
        .insert({
          email: fetchResult.email,
          google_access_token: accessToken,
          name: fetchResult.name, 
          given_name: fetchResult.given_name,
          family_name: fetchResult.family_name,
          picture: fetchResult.picture,
        })
        .into('users')
        .returning('*')
      user = users[0]
      delete user.google_access_token
      return user
    }

    // if this user already registered
    await this.knex('users')
      .update({ google_access_token: accessToken })
      .where({ id: user.id })

    user.google_access_token = accessToken
    delete user.password
    return user
  }
}
