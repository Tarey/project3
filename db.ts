import Knex from 'knex'
import pg from 'pg';
import { env } from './env';

let knexConfigs = require('./knexfile')
let mode = env.NODE_ENV

export const knex = Knex(knexConfigs[mode])

export let db = new pg.Client({
    database: env.DB_NAME,
    user: env.DB_USER,
    password: env.DB_PASS,
  })
  
  db.connect()