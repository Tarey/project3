import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {  // Deletes ALL existing entries
    await knex("users").del();

    // Inserts seed entries
    await knex("users").insert([
        {username: "john", email: "john@gmail.com", gender: "m", created_at: knex.fn.now(), bio: "12312541261"},
        {username: "peter", email: "peter@gmail.com", gender: "m", created_at: knex.fn.now(), bio: "sadasgshbdfbd"},
        {username: "stephen", email: "stephen@gmail.com", gender: "m", created_at: knex.fn.now(), bio: "eafdfgjghjfgdf"},
        {username: "bob", email: "bob123@gmail.com", gender: "m", created_at: knex.fn.now(), bio: "asdfszdfhfxgnb"},
        {username: "sophia", email: "ss3344@gmail.com", gender: "f", created_at: knex.fn.now(), bio: "asrweay453aytgr"},
        {username: "lucy", email: "lucyamazing@gmail.com", gender: "f", created_at: knex.fn.now(), bio: "asfsfdhxfgnbsad"},
        {username: "mike", email: "mm615@gmail.com", gender: "m", created_at: knex.fn.now(), bio: "234rwteausrthfnx"},
        {username: "michael", email: "mike666@gmail.com", gender: "m", created_at: knex.fn.now(), bio: "fsdzfhgjmhulikjd"},
        {username: "teemo", email: "teemo123@gmail.com", gender: "m", created_at: knex.fn.now(), bio: "342rtwesfa"}
    ]);
};
