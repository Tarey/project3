import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("friendship").del();

    // Inserts seed entries
    await knex("friendship").insert([
        { friend_at_date: knex.fn.now(), follower: "1", followering: "9", approved_friend: "true"},
        { friend_at_date: knex.fn.now(), follower: "1", followering: "8", approved_friend: "true"},
        { friend_at_date: knex.fn.now(), follower: "1", followering: "7", approved_friend: "true"},
        { friend_at_date: knex.fn.now(), follower: "2", followering: "6", approved_friend: "false"},
        { friend_at_date: knex.fn.now(), follower: "2", followering: "5", approved_friend: "false"},
        { friend_at_date: knex.fn.now(), follower: "2", followering: "4", approved_friend: "false"},
        { friend_at_date: knex.fn.now(), follower: "3", followering: "1", approved_friend: "false"},
        { friend_at_date: knex.fn.now(), follower: "3", followering: "2", approved_friend: "false"},
        { friend_at_date: knex.fn.now(), follower: "4", followering: "1", approved_friend: "false"},
        { friend_at_date: knex.fn.now(), follower: "4", followering: "3", approved_friend: "false"},
        { friend_at_date: knex.fn.now(), follower: "4", followering: "9", approved_friend: "false"}
    ]);
};
