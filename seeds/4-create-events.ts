import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("events").del();

    // Inserts seed entries
    await knex("events").insert([
        { eventlocation: knex.raw('POINT (22.28052, 114.15749)'), eventname: "party", description: "party at LKF 10pm", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 1},
        { eventlocation: knex.raw('POINT (22.27942, 114.17270)'), eventname: "dicount", description: "bakehouse big discounts!", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 2},
        { eventlocation: knex.raw('POINT (22.28009, 114.16469)'), eventname: "traffic", description: "traffic jam...", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 2},
        { eventlocation: knex.raw('POINT (22.27993, 114.18421)'), eventname: "SOGO", description: "new store near SOGO", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 2},
        { eventlocation: knex.raw('POINT (22.29347, 114.16975)'), eventname: "packed!", description: "way too many people in TST", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 3},
        { eventlocation: knex.raw('POINT (22.29494, 114.16740)'), eventname: "bookstore opened", description: "awesome books!", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 3},
        { eventlocation: knex.raw('POINT (22.30341, 114.17146)'), eventname: "locked down la!", description: "police sealed off the whole area!", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 3},
        { eventlocation: knex.raw('POINT (22.30505, 114.18471)'), eventname: "fresh fruits", description: "got some fresh fruits in the market", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 3},
        { eventlocation: knex.raw('POINT (22.32005, 114.16826)'), eventname: "checking in on a manga store", description: "no money ah...", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 4},
        { eventlocation: knex.raw('POINT (22.31805, 114.16799)'), eventname: "Eason Chan!", description: "Just saw Eason here!", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 4},
        { eventlocation: knex.raw('POINT (22.32985, 114.16260)'), eventname: "Just got off school", description: "where next, MK?", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 5},
        { eventlocation: knex.raw('POINT (22.30217, 114.15461)'), eventname: "sunset", description: "nice time for a walk", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 5},
        { eventlocation: knex.raw('POINT (22.28741, 114.14803)'), eventname: "tecky4life", description: "so wow, much teck", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 5},
        { eventlocation: knex.raw('POINT (22.28196, 114.14496)'), eventname: "wow", description: "way too many gold diggers here", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 5},
        { eventlocation: knex.raw('POINT (22.27961, 114.17653)'), eventname: "no bus...", description: "where is the bus...?", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 5},
        { eventlocation: knex.raw('POINT (22.29130, 114.19755)'), eventname: "cake shop is nice", description: "just bought a tart", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 6},
        { eventlocation: knex.raw('POINT (22.30234, 114.18176)'), eventname: "it's a maze", description: "Just got lost here... help...", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 6},
        { eventlocation: knex.raw('POINT (22.30656, 114.18100)'), eventname: "don't use the cross habor!", description: "way too packed around the toll", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 7},
        { eventlocation: knex.raw('POINT (22.32266, 114.17517)'), eventname: "hi", description: "hi", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 7},
        { eventlocation: knex.raw('POINT (22.32721, 114.17063)'), eventname: "swimming pool closed", description: "close at 6...", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 7},
        { eventlocation: knex.raw('POINT (22.33602, 114.17879)'), eventname: "jsut became a sardine", description: "fishy fishy", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 7},
        { eventlocation: knex.raw('POINT (22.28289, 114.13115)'), eventname: "studying", description: "library is empty", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 8},
        { eventlocation: knex.raw('POINT (22.28382, 114.13258)'), eventname: "also study", description: "what you studying?", created_at: knex.fn.now(), updated_at: knex.fn.now(), userid: 8},
    ]);
};
