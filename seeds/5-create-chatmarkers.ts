import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("chatmarker").del();

    // Inserts seed entries
    await knex("chatmarker").insert([
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "party", username: "john", created_at: "2021-02-06 10:10:31"},
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "here at LKF", username: "john", created_at: "2021-02-06 10:10:32"},
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "bring friends", username: "john", created_at: "2021-02-06 10:10:33"},
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "drinks 25% discount", username: "john", created_at: "2021-02-06 10:10:34"},
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "nice girls!", username: "john", created_at: "2021-02-06 10:10:35"},
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "haha", username: "john", created_at: "2021-02-06 10:10:36"},
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "another one?", username: "john", created_at: "2021-02-06 10:10:37"},
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "let's go", username: "john", created_at: "2021-02-06 10:10:38"},
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "wherer are you?", username: "john", created_at: "2021-02-06 10:10:39"},
        { chatlocation: knex.raw('POINT (22.28052, 114.15749)'), messages: "hi", username: "john", created_at: "2021-02-06 10:10:40"},
    ]);
};
