import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("usersloc").del();

    // Inserts seed entries
    await knex("usersloc").insert([
        {locid: "1", userid: "1", log_date: "2020-10-11 09:02:34", coords: "22.2456, 114.1761"},
        {locid: "2", userid: "2", log_date: "2020-11-12 11:10:34", coords: "22.2860, 114.2005"},
        {locid: "3", userid: "3", log_date: "2020-11-13 16:21:45", coords: "22.2532, 114.1814"},
        {locid: "4", userid: "4", log_date: "2020-11-17 12:11:01", coords: "22.2773, 114.1617"},
        {locid: "5", userid: "5", log_date: "2020-11-20 11:10:34", coords: "22.3302, 114.1901"},
        {locid: "6", userid: "6", log_date: "2020-12-15 20:10:37", coords: "22.2949, 114.1673"},
        {locid: "7", userid: "7", log_date: "2021-01-01 08:10:34", coords: "22.2813, 114.1604"},
        {locid: "8", userid: "8", log_date: "2021-01-02 13:09:12", coords: "22.2961, 114.1745"},
        {locid: "9", userid: "9", log_date: "2021-02-01 01:11:34", coords: "22.2818, 114.1876"},
        {locid: "10", userid: "2", log_date: "2021-02-05 14:17:04", coords: "222.3059, 114.1719"},
        {locid: "11", userid: "3", log_date: "2021-02-06 10:10:31", coords: "22.2818, 114.1876"},
    ]);
};