create user mapper with password 'mapper' superuser;

create database mapper;

create table users (
    id serial primary key,
    username VARCHAR(255) unique,
    email VARCHAR(255) unique,
    password VARCHAR(255),
    google_access_token VARCHAR(255)
);

alter TABLE users add COLUMN email VARCHAR(255) unique;
alter TABLE users add COLUMN google_access_token VARCHAR(255) unique;



-- use migrations for creating tables, code below is for record purpose.
-- create TABLE if not exists users (
--     id serial primary key,
--     username VARCHAR(255) unique not null,
--     email VARCHAR(255) unique not null,
--     password VARCHAR(255),
--     role VARCHAR(20),
--     created_at TIMESTAMP default now(),
--     updated_at TIMESTAMP,
--     logged_at TIMESTAMP
-- );
-- role VARCHAR(20) = gender