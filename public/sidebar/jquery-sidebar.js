/* global $ */
var map = L.map('map').setView([22.2800, 114.1588], 15);

//tile styling:
// http://leaflet-extras.github.io/leaflet-providers/preview/index.html

// NOTE: the hidden API below is paid service, do not activate unless for testing.
// L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
//     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     maxZoom: 18,
//     id: 'mapbox/streets-v11',
//     tileSize: 512,
//     zoomOffset: -1,
//     accessToken: 'pk.eyJ1IjoidGFyZXkiLCJhIjoiY2tubGh4Z2tlMGl0bjJvcGVmdDNod2dociJ9.KagirgX4r-qek77FyesuDg'
// }).addTo(map);

// L.tileLayer('https://stamen-tiles.a.ssl.fastly.net/toner-hybrid/{z}/{x}/{y}.png', {
//     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
//     minZoom: 4,
// }).addTo(map);

L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
    minZoom: 4,
    maxZoom: 20,
}).addTo(map);

// var CartoDB_Positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
// 	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
// 	minZoom: 4,
// 	maxZoom: 20,
// }).addTo(map);

// var Stamen_TonerLite = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}{r}.{ext}', {
// 	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
// 	minZoom: 4,
// 	maxZoom: 20,
// 	ext: 'png'
// }).addTo(map);

// initialise leaflet map
// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: 'Stamen',
//     minZoom: 4,
// }).addTo(map);

// popup-chat
// load username

let userProfile = {}

window.onload = async function getUserProfile() {
    let res = await fetch('/user/user-profile')
    let userProfileResult = await res.json()
    userProfile = userProfileResult
    return userProfile;
}

let content = `<p id="newContent"></p><div id="markercontainer"><form id="formElem"><input id="textarea" placeholder="please enter ur msg"><button id="submitForm" type="button" onclick="getContent()">Submit</button></form></div>`;

// get new contents and append new content to the content box
let coordinates;
function getContent(e) {
    let newContent = document.getElementById('textarea').value + "<span>@" + `${userProfile.given_name}` + "</span>" + "<br \>" + document.getElementById('newContent').innerHTML;
    document.getElementById('newContent').innerHTML = newContent; // get user value from input and add to the content table
    document.getElementById('textarea').value = '';
}

// popup - to be bound to the leaflet marker
var popup = L.responsivePopup().setContent(content)

// manuel control to add marker
let lastClicked = null;
map.on("contextmenu", function (e) {
    // icon settings
    coordinates = e.latlng
    let myIcon = L.icon({
        draggable: true,
        title: "This is the end!",
        opacity: 0.5,
        iconUrl: "https://blog.emojipedia.org/content/images/2020/09/EE09558D-C620-4FEA-94C8-7BC89C1881D3.gif",
        iconSize: [50, 50], // size of the icon
        iconAnchor: [25, 50], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -30] // point from which the popup should open relative to the iconAnchor
    })
    let button = document.querySelector('#setContentMarker');
    button.removeEventListener('click', lastClicked); // remove event listener after first click to avoid duplicate
    function onclick() {
        new L.Marker(e.latlng,
            { icon: myIcon })
            .addTo(map)
            .bindPopup(popup)
        // .bindPopup(map.removable, {
        //     removable: true,
        //     editable: true,
        // });
        button.removeEventListener('click', onclick);
    };
    lastClicked = onclick;
    button.addEventListener('click', onclick); // once click, it executes the function "onclick".
    console.log(coordinates)
    return coordinates
})
// end of popup chat

// icon settings
let color = '#fcfcf9';
let background = '#1F7499';
let icons = "school";
var iconics = document.getElementById("iconsSelection").value;
var colorRGB = document.getElementById("mycolorid").value;
var backRGB = document.getElementById("mybackgroundid").value;
let iconSelection = ["mapshakers",
    "mapkey",
    "bar",
    "traffic_signal",
    "playground_alt",
    "dentist",
    "fishing",
    "wind_generator",
    "crucifix",
    "cave_entrance",
    "castle_defensive",
    "cinema",
    "water_tower",
    "boundary_stone",
    "zoo",
    "cinema_alt",
    "court_house",
    "fort",
    "fountain",
    "library",
    "rocks",
    "cemetery",
    "pub",
    "buddhism",
    "triangle",
    "police",
    "ice_ring",
    "marina",
    "playground",
    "ruins",
    "internet",
    "airport",
    "chapel",
    "museum",
    "monument",
    "sport_centre",
    "information",
    "hospital",
    "golf",
    "picnic_site",
    "drag_lift",
    "construction",
    "islamism",
    "vineyard",
    "lighthouse",
    "tower",
    "wayside_cross",
    "theatre",
    "train",
    "viewpoint",
    "tram",
    "tree_cinofer",
    "university",
    "tram_stop",
    "underground",
    "adit",
    "post_office",
    "spring",
    "stadium",
    "swimming_pool",
    "cityhall",
    "bus",
    "volcano",
    "architecture",
    "arrow_up",
    "archaeological",
    "arrow_up_double",
    "building",
    "toilet",
    "watermill",
    "pharmacy",
    "waterpark",
    "viewtower",
    "city",
    "battlefield",
    "clean",
    "judaism",
    "christianism",
    "vcrown",
    "restaurant",
    "windmill",
    "caravan_site",
    "castle",
    "camp_site",
    "cable_car",
    "bus_stop",
    "bicycle",
    "drop",
    "beach",
    "water_well",
    "tree_leaf",
    "bag",
    "barracks",
    "laptop",
    "waterwork",
    "mountain",
    "home",
    "hostel",
    "hotel",
    "hotel_alt",
    "mall",
    "market_place",
    "cartography",
    "pitch",
    "shelter",
    "saddle",
    "sinkhole",
    "supermarket",
    "telephone",
    "stupa",
    "magnifier",
    "flag",
    "eye",
    "gallery",
    "gear",
    "map",
    "island",
    "marker",
    "nature",
    "square",
    "ticket",
    "star",
    "town",
    "village",
    "unesco",
    "facebook",
    "github",
    "linkedin",
    "skype",
    "data",
    "database",
    "analyse",
    "atm",
    "avatar",
    "burger",
    "cash",
    "contour",
    "download",
    "upload",
    "heart",
    "invisible",
    "school",
    "mine",
    "smartphone"];
var mki = L.icon.mapkey({
    icon: `${icons}`,
    color: `${color}`,
    background: `${background}`,
});

function getNumbers() {
    var player1 = new Array();
    var cardContainer = document.getElementById("iconsSelection");
    for (var i = 1; i <= 143; i++) {
        player1.push(iconSelection[i]);
        var option = document.createElement("option");
        option.innerHTML = (iconSelection[i]);
        cardContainer.appendChild(option);
    }
}
getNumbers();

document.getElementById("iconsSelection").onchange = function () {
    iconics = this.value;
    icons = iconics;
    console.log(iconics);
}

$('#mycolorid').hexColorPicker();
document.getElementById("mycolorid").onchange = function () {
    colorRGB = this.value;
    color = colorRGB;
    console.log(colorRGB);
}

$('#mybackgroundid').hexColorPicker();
document.getElementById("mybackgroundid").onchange = function () {
    backRGB = this.value;
    background = backRGB;
    console.log(backRGB);
}

function monkey() {
    var mki = L.icon.mapkey({ // this is a script from mapkeyicons folder
        icon: `${icons}`, // icon
        color: `${color}`, // color can be set from the sidebar
        background: `${background}`, // background can be set from the sidebar
    })
    console.log({ icons: mki })
    let coords = map.getCenter();
    const { lat, lng } = coords;
    L.marker([lat, lng], { icon: mki }).addTo(map).bindPopup(map.removable, {
        removable: true,
        editable: true,
    });
}

// set center from custom menu
map.on("contextmenu", function (e) {
    let locale = e.latlng
    document.querySelector('#center').addEventListener('click', () => {
        map.flyTo(locale, 18);
    });
});

// set marker from custom menu
let lastonclick = null;
map.on("contextmenu", function (e) {
    var mki = L.icon.mapkey({ // this is a script from mapkeyicons folder
        icon: `${icons}`, // icon
        color: `${color}`, // color can be set from the sidebar
        background: `${background}`, // background can be set from the sidebar
    })
    let button = document.querySelector('#setMarker')
    button.removeEventListener('click', lastonclick);
    function onclick() {
        new L.marker(e.latlng, { icon: mki }).addTo(map).bindPopup(map.removable, {
            removable: true,
            editable: true,
        });
        button.removeEventListener('click', onclick);
        // once "onclick" is executed, removeEventListener then executes.
        // as a result, only 1 eventlistener happens at any one time, no repeats.
    }
    lastonclick = onclick;
    button.addEventListener('click', onclick); // once click, it executes the function "onclick".
});

// look for user's current location on start up
navigator.geolocation.getCurrentPosition(function (location) {
    const latitude = location.coords.latitude;
    const longitude = location.coords.longitude;
    var latlng = new L.LatLng(latitude, longitude);
    console.log(latlng)
    map.flyTo(new L.LatLng(latitude, longitude));
});

// manually look for user's current location by clicking the GPS button
document.getElementById('button').addEventListener('click', function GPS() {
    navigator.geolocation.getCurrentPosition(function (location) {
        let latitude = location.coords.latitude;
        let longitude = location.coords.longitude;
        map.flyTo(new L.LatLng(latitude, longitude), 18);
    });
});

// side bar function - based on jQuery
$.fn.sidebar = function (options) {
    var $sidebar = this;
    var $tabs = $sidebar.find('ul.sidebar-tabs, .sidebar-tabs > ul');
    var $container = $sidebar.children('.sidebar-content').first();

    options = $.extend({
        position: 'left'
    }, options || {});

    $sidebar.addClass('sidebar-' + options.position);

    $tabs.children('li').children('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        var $tab = $(this).closest('li');

        if ($tab.hasClass('active'))
            $sidebar.close();
        else if (!$tab.hasClass('disabled'))
            $sidebar.open(this.hash.slice(1), $tab);
    });

    $sidebar.find('.sidebar-close').on('click', function () {
        $sidebar.close();
    });

    $sidebar.open = function (id, $tab) {
        if (typeof $tab === 'undefined')
            $tab = $tabs.find('li > a[href="#' + id + '"]').parent();

        // hide old active contents
        $container.children('.sidebar-pane.active').removeClass('active');

        // show new content
        $container.children('#' + id).addClass('active');

        // remove old active highlights
        $tabs.children('li.active').removeClass('active');

        // set new highlight
        $tab.addClass('active');

        $sidebar.trigger('content', { 'id': id });

        if ($sidebar.hasClass('collapsed')) {
            // open sidebar
            $sidebar.trigger('opening');
            $sidebar.removeClass('collapsed');
        }
    };

    /**
     * Close the sidebar (if necessary).
     */
    $sidebar.close = function () {
        // remove old active highlights
        $tabs.children('li.active').removeClass('active');

        if (!$sidebar.hasClass('collapsed')) {
            // close sidebar
            $sidebar.trigger('closing');
            $sidebar.addClass('collapsed');
        }
    };

    return $sidebar;
};
