// async function logout(){
//     let res = await fetch('/logout')
//     let status = res.status
//     if(status == '200'){
//         location.reload();
//     }
// }
  
async function logout() {
    let res = await fetch("/logout", { method: "POST" });
    window.location.href = "/";
}