
const socket = io.connect();
const chatbody = document.getElementById("chat_body")
const chatroom = document.getElementById("chatroom")
const chat_header = document.getElementById("chat_header")
const addchatroombox = document.getElementById("showaddchatroombox")
const chatfooter = document.querySelector('.chat_footer')
let useronline = false;
function scolltotop() { chatbody.scrollTop = chatbody.scrollHeight }




socket.on('connect', () => {
    console.log("connet to server");
    useronline = true
    console.log(" useronline =" + useronline);
})


const userId= [];
const chatrooms = [];
const lastchatroom = [null, null];
const chatroomids = [null]
const lastmessages = [];
const lastchat = [];
const username = [];


axios.get('/getuserprofile').then(res=>{
userId.push(res.data[0].id);
username.push(res.data[0].given_name)
})


axios.get('/getmessage').then(res => {

    lastmessages.push(res.data)

}).then(() => {
    for (let i = 0; i < lastmessages[0].length; i++) {
        lastchat.push({
            time: Number(lastmessages[0][i].time),
            message: lastmessages[0][i].message,
            chatroom_id: lastmessages[0][i].chatroom_id
        });
    }
})






function getroomid(roomid) {



    const chatroomid = chatroomids.pop()
    const roomname = document.getElementById("roomname" + roomid).value
    chatroomids.push(roomid)
    console.log(roomname)

    chatfooter.innerHTML = `
    <form id="chatForm">
                    <input placeholder="Type a message" type="text" id="chatInput" />
                    <button type="submit">Send a message</button>
                    <button type="" class="mic-icon"><i class="fas fa-microphone"></i></button>
                </form>
                `

    document.querySelector('#chatForm').addEventListener("click",
        async (e) => {
            e.preventDefault()
            if (document.querySelector('#chatInput').value != '') {
                let chatdata = {
                    "senderid": userId.join(),
                    "message": document.querySelector('#chatInput').value,
                    "name": username.join(),
                    "chatroom_id": chatroomids.join(),
                    "chatroom_name": roomname
                }
                // console.log(input);
                await axios.post("/postmessage", chatdata)
                    .then(document.querySelector('#chatInput').value = '');

            }
        }

    )

    chat_header.innerHTML = ''
    if (useronline) {
        chat_header.innerHTML = `
<div class="icon"><img class="img" height="80px" src="/img/icon.png"></div>
<div class="chat_headerinfo">
<h3>${roomname}${roomid}</h3>
<p>Online</p>
</div>
<div class="chat_headerright"

</div>`
    }



    axios.get('/getmessage')
        .then(function (response) {
            const datas = response.data
            console.log(datas);
            chatbody.innerHTML = ''
            for (let i = 0; i < datas.length; i++) {
                const data = datas[i];
                if (datas[i].chatroom_id == chatroomids.join()) {
                    if (data.senderid === Number(userId.join())) {
                        chatbody.innerHTML = chatbody.innerHTML + `<p class="chat_message chat_reciever">
   <span class="chat_name">
    ${data.name}
   </span>
   ${data.message}
   <span class="chat_timestamp">
    ${data.time}
    </span>

</p>
`
                    } else {
                        chatbody.innerHTML = chatbody.innerHTML + `<p class="chat_message">
        <span class="chat_name">
    ${data.name}
   </span>
   ${data.message}
   <span class="chat_timestamp">
    ${data.time}
    </span>

</p>`

                    }
                }
            }
        }).then(() => scolltotop())
        .then(() => { socket.emit('getroomid', chatroomids.join()) })
        .then(() => {
            lastchatroom.splice(0, 1)
            lastchatroom.push("roomname" + roomid)
            if (document.getElementById(lastchatroom[0]) != null) {
                document.getElementById(lastchatroom[0]).parentElement.classList.remove('chartroomonclick')
            }
            document.getElementById(lastchatroom[1]).parentElement.classList.add('chartroomonclick')

            console.log(lastchatroom);
        })


}



axios.get('/getchatroom')
    .then(function (response) {
        const datas = response.data
        for (let i = 0; i < datas.length; i++) {
            const data = datas[i];

            chatrooms.push({
                id: data.id, div: ` 
        <div class="sidebarchat" onclick="getroomid(${data.id})" id="${data.id}" >
            <input  style="display: none;" type="text" id="roomname${data.id}" value="${data.chatroom_name}"></input>
                    <div class="icon"><img class="img" height="80px" src="/img/icon.png"></div>
                    <div class="sidebarchat_info">
                        <h2>${data.chatroom_name}</h2>
                        <h2 id = "lm${data.id}">${lastmessage(data.id)}</h2>
                        <p>${data.id}</p>        
                    </div>

                </div>
        `})
            console.log(chatrooms);
            chatroom.innerHTML = chatroom.innerHTML + chatrooms[i].div
            console.log(chatrooms);
        }


    })






socket.on('addchatroom', chatroomdata => {
    chatrooms.splice(0, 0, {
        id: chatroomdata.id, div: ` 
        <div class="sidebarchat" onclick="getroomid(${chatroomdata.id})" id ="${chatroomdata.id}" >
            <input  style="display: none;" type="text" id="roomname${chatroomdata.id}" value="${chatroomdata.chatroom_name}"></input>
                    <div class="icon"><img class="img" height="80px" src="/img/icon.png"></div>
                    <div class="sidebarchat_info">
                        <h2>${chatroomdata.chatroom_name}</h2>
                        <h2 id = "lm${chatroomdata.id}">${lastmessage(chatroomdata.id)}</h2>
                        <p>${chatroomdata.id}</p>
                    </div>

                </div>
        `})
    console.log(chatrooms);
    let updatelist = [];
    for (let j = 0; j < chatrooms.length; j++) {
        updatelist.push(chatrooms[j].div)
        console.log(updatelist);

    }
    chatroom.innerHTML = updatelist.join()
})



if (document.querySelector('#postchatroom')) {
    document.querySelector('#postchatroom').addEventListener("click",
        (e) => {
            e.preventDefault()
            console.log("post chatroom")
            addchatroombox.innerHTML = ` <div class="chatroominput_box">
        <div class="addchatroom_crose"><i class="fas fa-times"></i></div>
       
<input placeholder="Type a chatroom name" type="text" id="AddChatRoomName" />
<input placeholder="Type a users id" type="number" id="Userid" />
<button onclick="addroom()">Send a message</button>
        </div >`
        }
    )
}


async function addroom() {
    console.log('add room')
    let chatroomdata = {
        "chatroom_name": document.querySelector('#AddChatRoomName').value,
        "room_createrid": userId.join(),
    }
    await axios.post("/postchatroom", chatroomdata)

}

socket.on('addchatroom', chatroomdata => {
    let userchatroomdata = {
        "user_id": Number(document.querySelector('#Userid').value),
        "chatrooms_id": chatroomdata.id
    }
    try {
        axios.post("/postuserschatroom", userchatroomdata)
        console.log(userchatroomdata)
        addchatroombox.innerHTML = ""
    }
    catch {
        console.log("can't post room user");
    }

})


socket.on('chatm', chatdata => {

    if (chatdata.chatroom_id === chatroomids.join()) {

        if (chatdata.senderid === userId.join()) {
            chatbody.innerHTML = chatbody.innerHTML + `<p class="chat_message chat_reciever">
<span class="chat_name">
${chatdata.name}
</span>
${chatdata.message}
<span class="chat_timestamp">
${chatdata.time}
</span>

</p>
`
            chatbody.scrollTop = chatbody.scrollHeight;
            console.log(socket.id);
        } else if
            (chatbody.scrollTop >= chatbody.scrollHeight - 2000) {
            console.log(chatbody.scrollTop);
            console.log(chatbody.scrollHeight);
            chatbody.scrollTop = chatbody.scrollHeight;
            chatbody.innerHTML = chatbody.innerHTML + `<p class="chat_message">
    <span class="chat_name">
        ${chatdata.name}
    </span>
${chatdata.message}
    <span class="chat_timestamp">
        ${chatdata.time}
    </span>

</p>`
        } else {
            chatbody.innerHTML = chatbody.innerHTML + `<p class="chat_message">
    <span class="chat_name">
${chatdata.name}
</span>
${chatdata.message}
<span class="chat_timestamp">
${chatdata.time}
</span>

</p>`
            chatbody.scrollTop = chatbody.scrollHeight;
        }

    }
}
)



function lastmessage(roomid) {
    try {
        filtermes = lastchat.filter(v => { return v.chatroom_id === roomid })
        console.log(filtermes[filtermes.length - 1].message);
        lastestmes = filtermes[filtermes.length - 1].message
        return lastestmes;
    } catch (er) {
        return "new room created !"
        console.log('new room');
        // console.error(e);
    }
}

socket.on('chatm', async chatdata => {

    for (let i = 0; i < chatrooms.length; i++) {

        if (chatrooms[i].id === Number(chatdata.chatroom_id)) {
            console.log("ok" + chatrooms[i].div);

            console.log(chatrooms[i]);

            chatrooms.splice(Number(i), 1)

            chatrooms.splice(0, 0, {
                id: Number(chatdata.chatroom_id), div: `
                <div class="sidebarchat" onclick="getroomid(${chatdata.chatroom_id})" id ="${chatdata.chatroom_id}" >
            <input  style="display: none;" type="text" id="roomname${chatdata.chatroom_id}" value="${chatdata.chatroom_name}"></input>
                    <div class="icon"><img class="img" height="80px" src="/img/icon.png"></div>
                    <div class="sidebarchat_info">
                        <h2>${chatdata.chatroom_name}</h2>
                        <h2 id = "lm${chatdata.chatroom_id}">${chatdata.message}</h2>
                        <p>${chatdata.chatroom_id}</p>
                    </div>

                </div>
                `})

            let updatelist = [];
            for (let j = 0; j < chatrooms.length; j++) {
                updatelist.push(chatrooms[j].div)


            }
        
            chatroom.innerHTML = updatelist.join()
            console.log(updatelist);
            break;

        } else {
            console.log("no ok");
        }
    }
    console.log("chatdata");
    console.log(chatdata.message, chatdata.chatroom_id);
})


