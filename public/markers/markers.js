// Place marker based on center view of the window

function manualMonkey(){ // this function is executed onclick from HTML
  let coords = map.getCenter();
  const { lat, lng } = coords;
  L.circle([lat,lng], {radius: 500}).addTo(map).bindPopup(map.removable, {
    removable: true,
    editable: true,
  }); // L.circle is a function from leaflet that generate a circle on the map
}


// TODO Jerry please add a delete function to remove the circle