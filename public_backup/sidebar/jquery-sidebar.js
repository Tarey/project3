/* global $ */
var map = L.map('map').setView([22.2800, 114.1588], 15);

//tile styling:
// http://leaflet-extras.github.io/leaflet-providers/preview/index.html

// NOTE: the hidden API below is paid service, do not activate unless for testing.
// L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
//     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     maxZoom: 18,
//     id: 'mapbox/streets-v11',
//     tileSize: 512,
//     zoomOffset: -1,
//     accessToken: 'pk.eyJ1IjoidGFyZXkiLCJhIjoiY2tubGh4Z2tlMGl0bjJvcGVmdDNod2dociJ9.KagirgX4r-qek77FyesuDg'
// }).addTo(map);

// L.tileLayer('https://stamen-tiles.a.ssl.fastly.net/toner-hybrid/{z}/{x}/{y}.png', {
//     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
//     minZoom: 4,
// }).addTo(map);

var CartoDB_Voyager = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
	minZoom: 4,
	maxZoom: 20,
}).addTo(map);

// var CartoDB_Positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
// 	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
// 	minZoom: 4,
// 	maxZoom: 20,
// }).addTo(map);

// var Stamen_TonerLite = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}{r}.{ext}', {
// 	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
// 	minZoom: 4,
// 	maxZoom: 20,
// 	ext: 'png'
// }).addTo(map);

// initialise leaflet map
// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: 'Stamen',
//     minZoom: 4,
// }).addTo(map);

// look for user's current location on start up
navigator.geolocation.getCurrentPosition(function (location) {
    const latitude = location.coords.latitude;
    const longitude = location.coords.longitude;
    var latlng = new L.LatLng(latitude, longitude);
    console.log(latlng)
    map.flyTo(new L.LatLng(latitude, longitude));
});

// manually look for user's current location by clicking the GPS button
document.getElementById('button').addEventListener('click', function () {
    navigator.geolocation.getCurrentPosition(function (location) {
        let latitude = location.coords.latitude;
        let longitude = location.coords.longitude;
        var latlng = new L.LatLng(latitude, longitude);
        console.log(latlng)
        map.flyTo(new L.LatLng(latitude, longitude), 18);
    });
});

// side bar function - based on jQuery
$.fn.sidebar = function (options) {
    var $sidebar = this;
    var $tabs = $sidebar.find('ul.sidebar-tabs, .sidebar-tabs > ul');
    var $container = $sidebar.children('.sidebar-content').first();

    options = $.extend({
        position: 'left'
    }, options || {});

    $sidebar.addClass('sidebar-' + options.position);

    $tabs.children('li').children('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        var $tab = $(this).closest('li');

        if ($tab.hasClass('active'))
            $sidebar.close();
        else if (!$tab.hasClass('disabled'))
            $sidebar.open(this.hash.slice(1), $tab);
    });

    $sidebar.find('.sidebar-close').on('click', function () {
        $sidebar.close();
    });

    $sidebar.open = function (id, $tab) {
        if (typeof $tab === 'undefined')
            $tab = $tabs.find('li > a[href="#' + id + '"]').parent();

        // hide old active contents
        $container.children('.sidebar-pane.active').removeClass('active');

        // show new content
        $container.children('#' + id).addClass('active');

        // remove old active highlights
        $tabs.children('li.active').removeClass('active');

        // set new highlight
        $tab.addClass('active');

        $sidebar.trigger('content', { 'id': id });

        if ($sidebar.hasClass('collapsed')) {
            // open sidebar
            $sidebar.trigger('opening');
            $sidebar.removeClass('collapsed');
        }
    };

    /**
     * Close the sidebar (if necessary).
     */
    $sidebar.close = function () {
        // remove old active highlights
        $tabs.children('li.active').removeClass('active');

        if (!$sidebar.hasClass('collapsed')) {
            // close sidebar
            $sidebar.trigger('closing');
            $sidebar.addClass('collapsed');
        }
    };

    return $sidebar;
};
