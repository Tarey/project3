window.onload = function loginLogout(){
    $.ajax({
      type: "GET",
      url: "/connect/google",
      success: function (data) {
        let $data = $("#login");
        $.each(data, function (i, data) {
          $data.append(
            '<button id="logout-button" class="btn" href=""><p>Logout <i class="material-icons">logout</i></p></button>'
          )
        })
      }
    })
  }