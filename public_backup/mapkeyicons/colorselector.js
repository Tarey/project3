let color = '#fcfcf9';
let background = '#1F7499';

function colorSelected (element) {
    color = element.value
    console.log(color)
}

function backgroundSelected (element) {
    background = element.value
    console.log(background)
}

function monkey(){
    var mki = L.icon.mapkey({
        icons: "school",
        color: `${color}`,
        background: `${background}`,
        size: 30,
        draggable: true,
    })
    L.marker([22.2874095, 114.14814779999999], { icon: mki }).addTo(map);}