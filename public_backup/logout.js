    
    
      let loginBtn = document.querySelector("#login")
      loginBtn.remove()
      let div = document.createElement('div')
      div.className = "button"
      div.id = "icon-container"
      let img = document.createElement('img')
      img.id = "user-icon"
      img.src = userDetail.profile_pic
      img.onclick = () => window.location = '/user/user-profile'
      let childDiv = document.createElement('div')
      childDiv.className = 'button-text-container'
      let p = document.createElement('p')
      p.id = 'login-text'
      p.textContent = 'Logout'
      childDiv.appendChild(p)
      div.appendChild(img)
      div.appendChild(childDiv)
      menu.appendChild(div)
  
      let logoutBtn = document.querySelector("#login-text")
  
      logoutBtn.addEventListener('click', () => {
        window.location = '/logout'
      })
  