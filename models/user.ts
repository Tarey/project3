

export const USER_TYPES = {
  FEMALE: "female",
  MALE: "make",
};



export type GoogleResult = {
  id: string
  email: string
  verified_email: boolean
  name: string
  given_name: string
  family_name: string
  picture: string
  locale: string
  hd: string
}
// export const defaultSomeType = {
//   id: {
//     type: string,
//     default: () => uuidv4().replace(/\-/g, ""),
//   },
// }

export type User = {
  id: string;
  email: string;
  google_access_token?: string;
  username: string;
  password?: string;
  created_at?: string;
  updated_at?: string;
}

export type chatmarker = {
  id: string;
  chatlocation: Geolocation;
  messages_usernames: JSON;
  created_at?: string;
  updated_at?: string;
}
// export class UserModel extends ImmuntableModel<UserModel>(
//   {
//     _id: {
//       type: String,
//       default: () => uuidv4().replace(/\-/g, ""),
//     },
//     firstName: String,
//     lastName: String,
//     type: String,
//   },
  
// )

// this file to be reviewed.