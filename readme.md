# Project3 (name to be determined) - suggestion: mapper

Warning: do not turn-on the Mapbox API script in leaflet.js except for testing - 
it is a limited paid service that will expire after 200,000 map requests.



### 1. Notes:
```
"npm i" to install all dependencies after every git pull.
```
```
review setup.sql and use knex migrations to setup database at your computers.
```
```
update your sql table by executing the following at the terminal "yarn knex migrate:latest"
```
```
.env requires the following to be executed for local data access:
initiate psql at terminal > create new user for database access by typing:
"create user mapper with password 'mapper' superuser;" and 
"create database mapper;"
```
```
db.ts, env.ts, guards.ts, hash.ts, controller, models, services are copied from sample documents in BAD004 -
to be reviewed for relevance and compatibility.
```
```
routes.ts CRUD operations require further development/consideration based on the app
```
```
junk-file folder is for dumping useless/backup files
```
```
jsfunctions folder is for reserve files to be used/studied later
```
```
please note that there is a duplicate css file in the css folder and public folder, 
there is a bug in the html where it must find the css file in both the css folder and public folder, 
this will be resolved later
```

### 2. Definitions:
```
role (setup.sql) = gender of the user
```
```
users = main SQL table for storing users' information
```
```
getUserByUserName = a function created in services for calling SQL username data, it is used again in controller for login function
```
```
point = a PostgreSQL type, used for storing GPS location on the database
```