// this function operates on click-event listener
// when user clicked on a the map, it produces a GPS coordinates.

"use strict";

L.Control.Coordinates = L.Control.extend({
	options: {
		position: 'bottomleft',
		latitudeText: 'lat.',
		longitudeText: 'lon.',
		promptText: 'Press Ctrl+C to copy coordinates',
		precision: 4
	},

	initialize: function(options)
	{
		L.Control.prototype.initialize.call(this, options);
	},

	onAdd: function(map)
	{
		var className = 'leaflet-control-coordinates',
			that = this,
			container = this._container = L.DomUtil.create('div', className);
		this.visible = false;

			L.DomUtil.addClass(container, 'hidden');


		L.DomEvent.disableClickPropagation(container);

		this._addText(container, map);

		L.DomEvent.addListener(container, 'click', function() {
			var lat = L.DomUtil.get(that._lat),
				lng = L.DomUtil.get(that._lng),
				latTextLen = this.options.latitudeText.length + 1,
				lngTextLen = this.options.longitudeText.length + 1,
				latTextIndex = lat.textContent.indexOf(this.options.latitudeText) + latTextLen,
				lngTextIndex = lng.textContent.indexOf(this.options.longitudeText) + lngTextLen,
				latCoordinate = lat.textContent.substr(latTextIndex),
				lngCoordinate = lng.textContent.substr(lngTextIndex);

			window.prompt(this.options.promptText, latCoordinate + ' ' + lngCoordinate);
    	}, this);

		return container;
	},

	_addText: function(container, context)
	{
		this._lat = L.DomUtil.create('span', 'leaflet-control-coordinates-lat' , container),
		this._lng = L.DomUtil.create('span', 'leaflet-control-coordinates-lng' , container);

		return container;
	},

	
	//  This method should be called when user clicks the map.
	setCoordinates: function(obj)
	{
		if (!this.visible) {
			L.DomUtil.removeClass(this._container, 'hidden');
		}

		if (obj.latlng) {
			L.DomUtil.get(this._lat).innerHTML = '<strong>' + this.options.latitudeText + ':</strong> ' + obj.latlng.lat.toFixed(this.options.precision).toString();
			L.DomUtil.get(this._lng).innerHTML = '<strong>' + this.options.longitudeText + ':</strong> ' + obj.latlng.lng.toFixed(this.options.precision).toString();
		}
	}
});

L.Control.MousePosition = L.Control.extend({
	options: {
	  position: 'bottomleft',
	  separator: ' : ',
	  emptyString: 'Unavailable',
	  lngFirst: false,
	  numDigits: 5,
	  lngFormatter: undefined,
	  latFormatter: undefined,
	  prefix: ""
	},
  
	onAdd: function (map) {
	  this._container = L.DomUtil.create('div', 'leaflet-control-mouseposition');
	  L.DomEvent.disableClickPropagation(this._container);
	  map.on('mousemove', this._onMouseMove, this);
	  this._container.innerHTML=this.options.emptyString;
	  return this._container;
	},
  
	onRemove: function (map) {
	  map.off('mousemove', this._onMouseMove)
	},
  
	_onMouseMove: function (e) {
	  var lng = this.options.lngFormatter ? this.options.lngFormatter(e.latlng.lng) : L.Util.formatNum(e.latlng.lng, this.options.numDigits);
	  var lat = this.options.latFormatter ? this.options.latFormatter(e.latlng.lat) : L.Util.formatNum(e.latlng.lat, this.options.numDigits);
	  var value = this.options.lngFirst ? lng + this.options.separator + lat : lat + this.options.separator + lng;
	  var prefixAndValue = this.options.prefix + ' ' + value;
	  this._container.innerHTML = prefixAndValue;
	}
  
  });
  
  L.Map.mergeOptions({
	  positionControl: false
  });
  
  L.Map.addInitHook(function () {
	  if (this.options.positionControl) {
		  this.positionControl = new L.Control.MousePosition();
		  this.addControl(this.positionControl);
	  }
  });
  
  L.control.mousePosition = function (options) {
	  return new L.Control.MousePosition(options);
  };