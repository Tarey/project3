import grant from 'grant'
import { createHttpTerminator } from 'http-terminator'
import express from 'express'
import util from 'util'
import fs from 'fs'
import expressExpress from 'express-session'
import path from 'path'
import { format } from 'fecha'
import dotenv from 'dotenv'
import http from 'http'
import socketIO from 'socket.io'
import { env } from './env'
import { setSocketIO } from './socketio'
import { knex } from './db'
import { isLoggedIn } from './guards'
import { createRouter } from './routes'
import multer from 'multer'
import { UserService } from './services/user-service'
import { UserController } from './controllers/user-controller'
import { profileRoute } from './user-profile'
dotenv.config()

// Socket/server initialise
let app = express()
let server = http.createServer(app)
let io = new socketIO.Server(server)
setSocketIO(io)
// end of socket/server initialisation

// npm package for terminating the HTTP server gracefully
const httpTerminator = createHttpTerminator({ server })

// express session
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// secret is a cryptographic key for encrypting the session ID. It should be in .env
// but for Tecky project purpose, it will be placed in main.ts for convenience.
let sessionMiddleware = expressExpress({
  secret: 'Memo Wall will not down',
  saveUninitialized: true,
  resave: true,
})
app.use(sessionMiddleware)

// A tag for AWS login - reverse proxy: there will only be a single port: 80, all visitors will be redirected to 8100
// Google login
const grantExpress = grant.express({
  defaults: {
    origin: `http://${env.HOST}:${env.PORT}`, // the port will be changed in AWS
    transport: 'session',
    state: true,
  },
  google: {
    key: env.GOOGLE_CLIENT_ID || '',
    secret: env.GOOGLE_CLIENT_SECRET || '',
    scope: ['profile', 'email'],
    callback: '/login/google', // warning, if loading non-stop, it means this callback has an issue.
  },
})

// Bind application-level middleware to an instance of the app object by using the app.use()
app.use(grantExpress as express.RequestHandler)

app.use((req, res, next) => {
  let counter = req.session['counter'] || 0
  counter++
  req.session['counter'] = counter
  next()
})

app.use((req, res, next) => {
  let date = new Date()
  let text = format(date, 'YYYY-MM-DD hh:mm:ss')
  console.log(`[${text}]`, req.method, req.url)
  next()
})

// admin
app.use(express.static('public'))
app.use(express.static('uploads_img'))
app.use('/admin', isLoggedIn, express.static('protected'))

// multer - for pictures upload
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads_img')
  },
  filename: (req, file, cb) => {
    let filename = file.fieldname + '-' + Date.now() + '-' + file.originalname
    cb(null, filename)
  },
})

let upload = multer({ storage })

// knex
let userService = new UserService(knex)

// Service
let userController = new UserController(userService)

// router
let router = createRouter({
  isLoggedIn,
  upload,
  userController,
})
app.use(router)

// TODO we need a 404 page - to be done later
app.use((req, res) => {
  res.sendFile(path.join(__dirname, 'public', '404.html'))
})

// Server initialisation - "npx ts-node-dev main.ts"
export async function main(PORT: number) {
  if (!fs.existsSync('uploads')) {
    fs.mkdirSync('uploads')
  }
  await new Promise<void>((resolve, reject) => {
    server.listen(PORT, () => {
      console.log(`listening on http://localhost:${PORT}`)
      resolve()
    })
    server.on('error', reject)
  })
  async function close() {
    console.debug('close io server')
    await util.promisify(io.close.bind(io))()

    console.debug('close knex client')
    await knex.destroy()

    console.debug('close http server')
    await httpTerminator.terminate()

    console.debug('closed all')
  }
  return { server, close }
}

// User profile information
app.use(profileRoute)

main(8100)
