import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {

    // table 1 - users information
    const users = await knex.schema.hasTable('users');
    if (!users) {
        await knex.schema.createTable('users', (table) => {
            table.increments("id").primary();
            table.string("username").unique();
            table.string("email").notNullable();
            table.string("name");
            table.string("given_name");
            table.string("family_name");
            table.string("picture");
            table.string("gender");
            table.timestamps(false, true);
            table.timestamp("logged_at");
            table.string("google_access_token");
            table.string("bio", 255);
            table.string("profilestatu", 255);
            table.string("followers", 255);
            table.string("following", 255);
        });
    };

     //1.2 - user_img
   const users_img = await knex.schema.hasTable('users_img');
   if (!users_img) {
       await knex.schema.createTable('users_img', (table) => {
           table.increments("id").primary();
           table.integer("userid").unsigned().references("id").inTable("users");
           table.string("img", 255);
       });
   };



    // table 2 - users geolocation
    const usersloc = await knex.schema.hasTable('usersloc');
    if (!usersloc) {
        await knex.schema.createTable('usersloc', (table) => {
            table.increments("locid");
            table.specificType("coords", 'POINT');
            table.integer("userid").unsigned().references("id").inTable("users");
            table.timestamp("log_date");
        });
    };

    // table 3 - friends/likes
    const friendship = await knex.schema.hasTable('friendship');
    if (!friendship) {
        await knex.schema.createTable('friendship', (table) => {
            table.timestamp("friend_at_date");
            table.integer("follower").unsigned().references("id").inTable("users");
            table.integer("followering").unsigned().references("id").inTable("users");
            table.boolean("approved_friend").defaultTo(false);
        })
    };

    // table 4 - chatroom
    const chatroom = await knex.schema.hasTable('chatroom');
    if (!chatroom) {
        await knex.schema.createTable('chatroom', (table) => {
            table.increments("id");
            table.string("chatroom_name", 255);
            table.integer("room_createrid").unsigned().references("id").inTable("users");
            table.string("time", 255);
        })
    }

    // table 5 - chat
    const messages = await knex.schema.hasTable('messages');
    if (!messages) {
        await knex.schema.createTable('messages', (table) => {
            table.increments("id");
            table.integer("senderid").unsigned().references("id").inTable("users");
            table.string("message", 255);
            table.string("name", 255);
            table.string("time", 255);
            table.integer("chatroom_id").unsigned().references("id").inTable("chatroom");
        })
    }

    // table 6 - chatroom_message
    const users_chatroom = await knex.schema.hasTable('users_chatroom');
    if (!users_chatroom) {
        await knex.schema.createTable('users_chatroom', (table) => {
            table.integer("chatrooms_id").unsigned().references("id").inTable("chatroom");
            table.integer("user_id").unsigned().references("id").inTable("users");
        })
    }

    // table 7 - current events
    const events = await knex.schema.hasTable('events');
    if (!events){
        await knex.schema.createTable('events', (table) => {
            table.increments("eventid");
            table.specificType("eventlocation", 'POINT');
            table.string("eventname");
            table.string("description", 255);
            table.timestamps(false, true);
            table.integer("userid").unsigned().references("id").inTable("users");
        })
    }

    // table 8 - map GPS chat
    const chatmarker = await knex.schema.hasTable('chatmarker');
    if (!chatmarker) {
        await knex.schema.createTable('chatmarker', (table) => {
            table.increments("chatid");
            table.specificType("chatlocation", 'POINT');
            table.jsonb("messages_usernames"); // array of JSON
            // table.string("username").references("username").inTable("users");
            table.timestamps(false, true);
        })
    }
    
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("friendship");
    await knex.schema.dropTableIfExists("usersloc");
    await knex.schema.dropTableIfExists("users");
    await knex.schema.dropTableIfExists("chatroom");
    await knex.schema.dropTableIfExists("messages");
    await knex.schema.dropTableIfExists("users_chatroom");
    await knex.schema.dropTableIfExists("events");
    await knex.schema.dropTableIfExists("chatmarker");
}

