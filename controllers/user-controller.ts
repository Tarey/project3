import { UserService } from '../services/user-service'
import express from 'express'
import { knex } from '../db'
import { io  } from '../socketio'
// import { servicesVersion } from 'typescript'

export class UserController {

  constructor(private userService: UserService) { }

  postuserimg =  async (req: express.Request, res: express.Response)=>{
    console.log(req.file.filename)
  let data ={
          img : req.file.filename,
          userid: req.session['user'].id  }
        
          io.emit('socketphtot',data)
  
          if(data){
            knex('users_img').insert(
              data
             )
             .then( function (result) {
               res.json({ success: true, message: "ok"  }).end();     // respond back to request
            }).catch((e)=>{console.log(e);
            })        
          } 
        }
  
  
    getuserimg=  async (req: express.Request, res: express.Response)=>{
      let result = await knex.select("*")
                    .from('users_img')
                    .where("userid" , req.session['user'].id)
                    .orderBy("id","desc")
                    .returning('*')
      res.send(result).end()
    }
  
  
  postfollowstatus =  async (req: express.Request, res: express.Response)=>{
    let follower = req.body.follower
    let followering = req.body.followering
    
    let Data = {
      follower: follower,
      followering: followering,
    }
    console.log(Data);
    
    if(Data){
      knex('friendship').insert(
        Data
       )
       .then( function (result) {
         res.json({ success: true, message: "ok"  }).end();     // respond back to request
      }).catch((e)=>{console.log(e);
      })        
    } 
}


getuserfollower =  async (req: express.Request, res: express.Response)=>{
  let result = await knex.select("follower")
                .from('friendship')
                .where("followering" ,  req.session['user'].id)
                .returning('*')
  res.send(result).end()
}

getuserfollowing =  async (req: express.Request, res: express.Response)=>{
  let result = await knex.select("followering")
                .from('friendship')
                .where("follower" ,  req.session['user'].id)
                .returning('*')
  res.send(result).end()
}




//get user profile
getuserprofile =  async (req: express.Request, res: express.Response)=>{
    let result = await knex.select("*")
                  .from('users')
                  .where("id" ,  req.session['user'].id)
                  .returning('*')
    res.send(result).end()
  }

  // chat function
  postchatroom =  async (req: express.Request, res: express.Response)=>{
    let chatroom_name = req.body.chatroom_name
    let room_createrid = req.body.room_createrid
    
    let chatroomDataA = {
      chatroom_name: chatroom_name,
      room_createrid: room_createrid,
      time: new Date()}

      if(chatroom_name){
        knex('chatroom').insert(
          chatroomDataA 
         )
         .then( function (result) {
           res.json({ success: true, message: 'ok' }).end();     // respond back to request
        }).catch((e)=>{console.log(e);
        })
        .then(async ()=>{
          let result = await knex.select('*').from('chatroom').returning('*')
          console.log('result = ', result.pop() );
     
          let socketchatroomData = {
            id: result.pop().id + 1 ,
            chatroom_name: chatroom_name,
            room_createrid: room_createrid,
            time: new Date()}
    
          io.emit('addchatroom', socketchatroomData )
        }
        )
      }else{
        console.log('name null');
        
      }
     
    // console.log({name,message,senderid});
     
  }

  postuserschatroom =  async (req: express.Request, res: express.Response)=>{
    let chatrooms_id = req.body.chatrooms_id
    let user_id = req.body.user_id
    
    let userchatroomData = {
      chatrooms_id: chatrooms_id,
      user_id: user_id,
    }

    io.emit('userchatroomData', userchatroomData)

    

      if(userchatroomData){
        knex('users_chatroom').insert(
          userchatroomData
         )
         .then( function (result) {
           res.json({ success: true, message: 'ok' }).end();     // respond back to request
        }).catch((e)=>{console.log(e);
        })        
      } 
  }

  getchatroom =  async (req: express.Request, res: express.Response)=>{
    let result = await knex.select("chatroom.id","chatroom.chatroom_name")
                  .from('users')
                  .innerJoin('users_chatroom','users.id',' user_id')
                  .innerJoin('chatroom','chatrooms_id','chatroom.id')
                  .where("user_id", req.session['user'].id)
                  .orderBy("chatroom.id")
                  .returning('*')
                 
                  
    // io.emit('addchatroom', result )
    res.send(result).end()
    console.log(req.session['user'])
  }


  getchat =  async (req: express.Request, res: express.Response)=>{
    let result = await knex.select("username" ,"chatroom_id" ,'messages.id','messages.time' ,'messages.name' ,'senderid' ,'chatroom_name' ,'message')
                  .from('users')
                  .innerJoin('users_chatroom','users.id',' user_id')
                  .innerJoin('chatroom','chatrooms_id','chatroom.id')
                  .innerJoin('messages','chatroom.id','chatroom_id')
                  .where("user_id",  req.session['user'].id)
                  .orderBy("messages.id")
                  .returning('*')
   
    // console.log('result = ', result);
    res.send(result).end()
  }



  postchat =  async (req: express.Request, res: express.Response)=>{
    let name = req.body.name
    let message = req.body.message
    let senderid = req.body.senderid
    let chatroom_id = req.body.chatroom_id
    let chatroom_name = req.body.chatroom_name

    let chatdata = {
      senderid: senderid,
      name: name,
      message : message,
      chatroom_id: chatroom_id,
      time: Date.now()}
     
      console.log(chatdata);
        

    let socketdatas = {
      senderid: senderid,
      name: name,
      message : message,
      chatroom_id: chatroom_id,
      time: Date.now(),
      chatroom_name: chatroom_name
    }

        io.emit('chatm', socketdatas)
    
    if(name){
      knex('messages').insert(
        chatdata
       ).then( function (result) {
         res.json({ success: true, message: 'ok' }).end();     // respond back to request
      }).catch((e)=>{console.log(e);
      })
    }else{
      console.log('name null');
      
    }
  }


   // end of chat function

  loginWithGoogle = async (req: express.Request, res: express.Response) => {
    const accessToken = req.session?.['grant'].response.access_token
    let user = await this.userService.createOrUpdateGoogleUser(accessToken)
    req.session['user'] = user
    res.redirect('/home.html')
    if (!accessToken) {
      res.status(400).end('missing accessToken')
      return
    }
  }

  // get chat markers information (global)
  getChatMarker = async (req: express.Request, res: express.Response ) => {
    let result = await this.userService.getChatMarkerInformation() 
    res.json(result);
  }
  
//user-profile
getUserProfile = async(req: express.Request, res: express.Response) => {
  let user = req.session['user']
  if (user){
    res.json(user)
  }else{
     res.json({})
  }
}


  // postChatMarker = async (req: express.Request, res: express.Response) => {
    
  // }
  
  // get all user location
  getUserLoc = async(req: express.Request, res: express.Response) => {
    let usersloc = req.body['locid']
    if (usersloc) {
      res.json(usersloc)
    }
    res.json({})
  }
  
  // logout function
  logout = (req: express.Request, res: express.Response) => {
    req.session['user'] = false
    res.status(200).end()
  }

  getRole = async(req: express.Request, res: express.Response) => {
    if (!req.session || !req.session['user']) {
      res.end('guest')
      return
    }
    if (req.session['user'].role === 'admin'){
      res.end('admin')
      return
    }
    res.end('normal')
  }
  getUserDetail = async(req: express.Request, res: express.Response) => {
    console.log('user',req.session['user'])
  
    res.json(req.session['user'])
  }

  getEvents = async(req: express.Request, res: express.Response) => {
    let result = await knex.select('*').from('events').returning('*')
    res.send(result).end()
  }
}


