import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema
    .alterTable('users', (table) => {
      table.string('username').nullable().alter();
      table.string('password').nullable().alter();
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema
    .alterTable('users', (table) => {
      table.string('username').notNullable().alter();
      table.string('password').notNullable().alter();
    });
}

