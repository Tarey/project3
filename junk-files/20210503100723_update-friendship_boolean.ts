import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('friendship')){
        await knex.schema.alterTable('friendship',(table)=>{
            table.boolean("approved_friend");
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('friendship')){
        await knex.schema.alterTable('friendship',(table)=>{
            table.boolean("approved_friend");
        });  
    }
}

