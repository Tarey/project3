import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('<table name>', table => {
        table.string('bio', 255);
      })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('<table name>', table => {
        table.dropColumn('bio');
      })

}

