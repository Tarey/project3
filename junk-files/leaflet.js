// set default view location for the user on-load of HTML.
var mymap = L.map('mapid').setView([51.505, -0.09], 13);



// NOTE: the hidden API below is paid service, do not activate unless for testing.
// L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
//     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     maxZoom: 18,
//     id: 'mapbox/streets-v11',
//     tileSize: 512,
//     zoomOffset: -1,
//     accessToken: 'pk.eyJ1IjoidGFyZXkiLCJhIjoiY2tubGh4Z2tlMGl0bjJvcGVmdDNod2dociJ9.KagirgX4r-qek77FyesuDg'
// }).addTo(mymap);






// NOTE: this API is free for use.
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

// sidebar
var sidebar = L.control.sidebar('sidebar').addTo(mymap);

// testing markers on the map: circle, marker, and triangle.
var marker = L.marker([51.5, -0.09]).addTo(mymap);
var circle = L.circle([51.508, -0.11], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);
var polygon = L.polygon([
    [51.509, -0.08],
    [51.503, -0.06],
    [51.51, -0.047]
]).addTo(mymap);