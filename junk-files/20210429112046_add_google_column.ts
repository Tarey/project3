import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('users', function (table) {
        table.string('google_access_token');
      })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('users', table => {
        table.dropColumn('google_access_token');
      })
}

