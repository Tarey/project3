import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('usersloc')){
        await knex.schema.alterTable('usersloc',(table)=>{
            table.dropColumn("locid");
        });
        await knex.schema.alterTable('usersloc',(table) => {
            table.increments("locid");
        })  
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('usersloc')){
        await knex.schema.alterTable('usersloc',(table)=>{
            table.dropColumn("locid");
        }); 
        await knex.schema.alterTable('usersloc',(table) => {
            table.increments("locid");
        })
    }
}

