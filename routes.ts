import express from 'express'
import { Multer } from 'multer'
import { UserController } from './controllers/user-controller'


// use the isLoggedIn guard to make sure only admin can call this API
export function createRouter(options: {
  upload: Multer
  isLoggedIn: express.RequestHandler
  userController: UserController
}) {
  const { upload, isLoggedIn, userController } = options
  if (false) {
    console.log(upload, isLoggedIn)
  }

  let router = express.Router()

  // CRUD operations - to be developed 
  // router.post('/', isLoggedIn, upload.single('image'), ...) // create, sample image uplaod
  // router.get('/user', isLoggedIn, userController.getUserProfile)
  // router.get('/user/user-profile', isLoggedIn, ...) // read
  // router.patch('/', isLoggedIn, ...) // update
  // router.delete('/', isLoggedIn, ...) // delete
  // router.get('/users',userController.getUserProfile)
  // user routes
  router.get('/login/google', userController.loginWithGoogle)
  router.get('/logout', userController.logout)
  // router.get('/users', userController.getUserProfile)
  // router.get('/chatmarker', userController.getChatMarker)
  // router.post('/login', userController.login)
  // router.post('/register', userController.register)
  router.get('/user/user-profile', userController.getUserProfile)
  // router.get('/userloc', userController.getUserLoc)
  router.get('/role', userController.getRole)
  router.get('/user-detail', userController.getUserDetail)
  router.get('/events', userController.getEvents)
    //chat routes
    router.get('/getmessage',userController.getchat);
    router.get('/getchatroom',userController.getchatroom);
    router.post('/postmessage',userController.postchat);
    router.post('/postchatroom',userController. postchatroom);
    router.post('/postuserschatroom',userController. postuserschatroom);


  //userprofile
    router.get('/getuserprofile',userController. getuserprofile);
    router.post('/postfollowstatus',userController. postfollowstatus);

    router.get('/getuserfollowing',userController. getuserfollowing);
    router.get('/getuserfollower',userController. getuserfollower);
    router.get('/getuserimg',userController. getuserimg);

  

  router.post('/form', upload.single('file'), userController.postuserimg)
 
  return router
}
